//
//  AppFont.swift
//  Vebrary
//
//  Created by DINH VAN TIEN on 5/6/19.
//  Copyright © 2019 DINH VAN TIEN. All rights reserved.
//

import UIKit

struct AppFont {
    static let fontBold15 = UIFont.boldSystemFont(ofSize: 15)
    static let fontItaly15 = UIFont.italicSystemFont(ofSize: 15)
    static let fontRegular15 = UIFont.systemFont(ofSize: 15)

    static let fontBold13 = UIFont.boldSystemFont(ofSize: 13)
    static let fontItaly13 = UIFont.italicSystemFont(ofSize: 13)
    static let fontRegular13 = UIFont.systemFont(ofSize: 13)
}
