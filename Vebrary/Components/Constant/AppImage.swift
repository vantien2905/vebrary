//
//  AppImage.swift
//  Vebrary
//
//  Created by DINH VAN TIEN on 5/6/19.
//  Copyright © 2019 DINH VAN TIEN. All rights reserved.
//

import UIKit

struct AppImage {
    static let imgMenu = UIImage(named: "menu")!
    static let imgBack = UIImage(named: "back")!
    static let imgAvata = UIImage(named: "avata")!
    static let imgHome = UIImage(named: "home")!
    static let imgIntrodure = UIImage(named: "introdure")!
    static let imgNews = UIImage(named: "news")!
    static let imgSearch = UIImage(named: "search")!
    static let imgFolder = UIImage(named: "folder")!
    static let imgRightArrow = UIImage(named: "right_arrow")!
}
