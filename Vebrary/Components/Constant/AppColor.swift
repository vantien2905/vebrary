//
//  AppColor.swift
//  Vebrary
//
//  Created by DINH VAN TIEN on 5/6/19.
//  Copyright © 2019 DINH VAN TIEN. All rights reserved.
//

import UIKit

struct AppColor {
    static let mainColor = UIColor(red: 74/255, green: 144/255, blue: 226/255, alpha: 1)
}
