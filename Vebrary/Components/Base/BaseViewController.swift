//
//  BaseViewController.swift
//  Vebrary
//
//  Created by DINH VAN TIEN on 5/6/19.
//  Copyright © 2019 DINH VAN TIEN. All rights reserved.
//

import Foundation
import UIKit

enum StyleNavigation {
    case left
    case right
}

class BaseViewController: UIViewController {

    let mainBackgroundColor = UIColor.white
    let mainNavigationBarColor = AppColor.mainColor

    override func viewDidLoad() {
//        super.viewDidLoad()
        setUpNavigation()
        setUpViews()
    }

    let lbNoData: UILabel = {
        let lb = UILabel()
        lb.text = "Không có dữ liệu"
        lb.font = AppFont.fontRegular15
        lb.textColor = .black
        return lb
    }()

    func showNoData() {
        lbNoData.removeFromSuperview()
        self.view.addSubview(lbNoData)
        lbNoData.centerSuperview()
    }

    func hideNoData() {
        lbNoData.removeFromSuperview()
    }

    let btnNavi : UIButton = UIButton()

    func setUpViews(){}

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    func setUpNavigation() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        guard let navigationController = self.navigationController else { return }
        //---
        navigationController.navigationBar.barTintColor = mainNavigationBarColor
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isHidden = false
    }

    func setDefaultNavi() {
        addButtonImageToNavigation(image: AppImage.imgMenu, style: .left, action: #selector(goBack))
        addButtonImageToNavigation(image: AppImage.imgAvata, style: .right, action: #selector(infoTapped))
    }

    @objc func goBack() {
        self.sideMenuController?.revealMenu()
    }

    @objc func infoTapped() {
        print("info user")
        let vc = UserInfoRouter.createModule()
        self.navigationController?.present(controller: vc)
//        self.present(controller: vc)
    }

    func showNavigation() {
        self.navigationController?.navigationBar.isHidden = false
    }

    func hideNavigation() {
        self.navigationController?.navigationBar.isHidden = true
    }
}

// MARK: Add button left, right, title
extension BaseViewController {
    func addButtonToNavigation(image: UIImage, style: StyleNavigation, action: Selector?) {
        let btn = UIButton()
        btn.setImage(image, for: .normal)
        if let _action = action {
            btn.addTarget(self, action: _action, for: .touchUpInside)
        }

        btn.frame = CGRect(x: 0, y: 0, width: 60, height: 44)

        let button = UIBarButtonItem(customView: btn)
        if style == .left {
            btn.contentHorizontalAlignment = .left
            self.navigationItem.leftBarButtonItem = button
        } else {
            self.navigationItem.rightBarButtonItem = button
            btn.contentHorizontalAlignment = .right
        }
    }

    func setTitleImageNavigation(image: UIImage) {
        let view = UIView()
        let imageView = UIImageView(image:image)
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        imageView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 9, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.navigationItem.titleView = view
    }

    func addTwoButtonToNavigation(image1: UIImage, action1: Selector?, image2: UIImage, action2: Selector?) {
        let btn1 = UIButton()
        btn1.setImage(image1, for: .normal)
        if let _action = action1 {
            btn1.addTarget(self, action: _action, for: .touchUpInside)
        }

        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        btn1.contentHorizontalAlignment = .right
        let button1 = UIBarButtonItem(customView: btn1)

        //---
        let btn2 = UIButton()
        btn2.setImage(image2, for: .normal)
        if let _action = action2 {
            btn2.addTarget(self, action: _action, for: .touchUpInside)
        }
        btn2.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        btn2.contentHorizontalAlignment = .right

        let button2 = UIBarButtonItem(customView: btn2)

        self.navigationItem.rightBarButtonItems = [button1, button2]
    }

    func addButtonTextToNavigation(title: String, style: StyleNavigation, action: Selector?, textColor: UIColor = UIColor.white) {

        var _title = title
        if style == .right {
            _title = title
        }
        btnNavi.setTitle(_title, for: .normal)
        btnNavi.setTitleColor(textColor, for: .normal)
        btnNavi.titleLabel?.font = UIFont.systemFont(ofSize: 17)

        if let _action = action {
            btnNavi.addTarget(self, action: _action, for: .touchUpInside)
        }
        btnNavi.sizeToFit()

        let button = UIBarButtonItem(customView: btnNavi)
        if style == .left {
            self.navigationItem.leftBarButtonItem = button
        } else {
            self.navigationItem.rightBarButtonItem = button
        }
    }

    func setTitleNavigation(title: String, textColor: UIColor = UIColor.white, action: Selector? = nil ) {
        let lb = UILabel()
        lb.font = AppFont.fontBold15
        lb.text             = title
        lb.textAlignment    = .center
        lb.numberOfLines    = 2
        lb.textColor        = textColor
        lb.sizeToFit()

        let tap = UITapGestureRecognizer(target: self, action: action)
        lb.addGestureRecognizer(tap)
        let vTest = UIView()
        vTest.frame = CGRect(x: 0, y: 0, width: 200, height: 44)
        vTest.addSubview(lb)
        lb.centerSuperview()
        self.navigationItem.titleView = vTest
    }

    func addButtonImageTitleToNavigation(image: UIImage = AppImage.imgBack, style: StyleNavigation = .left, title: String, action: Selector? = #selector(btnBackTapped)) {
        let btn = UIButton()
        btn.setImage(image, for: .normal)
        if let _action = action {
            btn.addTarget(self, action: _action, for: .touchUpInside)
        }
        btn.frame = CGRect(x: 0, y: 0, width: 370, height: 44)
        btn.setTitle(title, for: .normal)

        let spacing: CGFloat = 10 // the amount of spacing to appear between image and title
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)

        btn.imageView?.contentMode = .scaleAspectFit
        let button = UIBarButtonItem(customView: btn)
        if style == .left {
            btn.imageEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 20)
            btn.contentHorizontalAlignment = .left
            self.navigationItem.leftBarButtonItem = button
        } else {
            btn.imageEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 0)
            self.navigationItem.rightBarButtonItem = button
            btn.contentHorizontalAlignment = .right
            self.navigationItem.rightBarButtonItem = button
        }
    }


    func addButtonImageToNavigation(image: UIImage, style: StyleNavigation, action: Selector?) {
        let btn = UIButton()
        btn.setImage(image, for: .normal)
        if let _action = action {
            btn.addTarget(self, action: _action, for: .touchUpInside)
        }
        btn.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        btn.imageView?.contentMode = .scaleAspectFit
        let button = UIBarButtonItem(customView: btn)
        if style == .left {
            btn.imageEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 20)
            btn.contentHorizontalAlignment = .left
            self.navigationItem.leftBarButtonItem = button
        } else {
            btn.imageEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 0)
            self.navigationItem.rightBarButtonItem = button
            btn.contentHorizontalAlignment = .right
            self.navigationItem.rightBarButtonItem = button
        }
    }
}
extension BaseViewController {
    func addBackWhiteToNavigation() {
        addButtonImageToNavigation(image: AppImage.imgBack, style: .left, action: #selector(btnBackTapped))
    }

    func addBackBlackToNavigation() {
        addButtonImageToNavigation(image: AppImage.imgBack, style: .left, action: #selector(btnBackTapped))
    }

    @objc func btnBackTapped() {
        self.pop()
    }

//    func addMainLogo() {
//        setTitleImageNavigation(image: AppImage.imgLogo)
//    }
}
