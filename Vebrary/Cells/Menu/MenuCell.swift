//
//  MenuCell.swift
//  LeftMenuDemo
//
//  Created by DINH VAN TIEN on 5/4/19.
//  Copyright © 2019 DINH VAN TIEN. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
